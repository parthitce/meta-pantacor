# meta-pantacor Image Build

## Required packages on Ubuntu

This section provides required packages on an Ubuntu Linux distribution:

### Essentials

Packages needed to build an image for a headless system:

```shell
$ sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
    build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
    xz-utils debianutils iputils-ping python-git repo bmap-tools
```

## Quick step

```shell
$ repo init -u ssh://git@gitlab.com:parthiban.nallathambi/meta-pantacor.git -m conf/samples/beaglebone-yocto/pv-beaglebone.xml -b master ; repo sync -j10; cd beagle/; TEMPLATECONF=meta-pantacor/conf/samples/beaglebone-yocto source oe-init-build-env; bitbake pv-image-initramfs
```

## Download meta-pantacor project manifest

To easily manage different git repositories layers, meta-pantacor project is using [Android repo tool](https://source.android.com/source/using-repo),

First initialize repo specifying the project manifest and the corresponding branch:

```shell
$ repo init -u ssh://git@gitlab.com:parthiban.nallathambi/meta-pantacor.git -m conf/samples/beaglebone-yocto/pv-beaglebone.xml -b master
```

then checkout the project source tree:

```shell
$ repo sync -j10
```

## Configuring the project

meta-pantacor offers pre-configured machine templates, tested and ready to use.

- beaglebone black

```shell
$ cd beagle/
$ TEMPLATECONF=meta-pantacor/conf/samples/beaglebone-yocto source oe-init-build-env
```

## Build the project

### SD card image
```shell
$ bitbake pv-image-initramfs
```

## Current status:
Working/Done:
-------------
IMX:
- Machine: meta-freescale-3rdparty collibri-imx6ull.conf
	- meta-freescale, meta-freescale-3rdparty, poky
- u-boot: NAND/SD - 2020.07 (mainline)
	- Manually compiled from source
- Linux Kernel: 4.9.x vendor from meta-freescale (zImage working)
	- Compiled from yocto
- dtb: from vendor as it is
	- Compiled from yocto
- pv-initramfs: yocto based should work ideally (yet to cross check once more)
	- initramfs compressed with xz
	- compiled from yocto
	- boots fine with zImage
- PVR_MERGE_SRC: Use the ARM generic initramfs and working fine
	- PVR source merge can be pulled directly with this variable
- pv-image: booting sysVinit based image fine
	- Template/sample image is built for local testing
- Random RFS tarball: mksqusfs on img is working fine
	- Downloaded for colibri RFS and manually run squasfs to run

TODO:
- u-boot 2016.11 (vendor) - YTD
	- vendor tree based u-boot compiles fine with Yocto
	- boot test done with RAW NAND and bcb layout is failed
	- Yet to test with SD card
	- Fix the booting from NAND
- u-boot env is not prepared yet (.env needed)
	- Pantacor env needs to be auto prepared from Yocto yet
- Kernel: uImage loadaddr and entry point address needs to be indentified
	- Alchamey build produces uImage and yocto produces zImage
	- uImage load address and entry point address for imx needs to fed
	  into Yocto
- Kernel: defconfig always uses from linux-toradex, don't know why?
	- Current layering in Yocto uses defconfig always from vendor layer
	- Defconfig fragment from meta-pantacor is not picked, yet to find the reason
	- defconfig from meta-pantcor isn't picked either, yet to find
- pv-image: pvr device scan (imx6) not yet working
	- Container boots fine without any errors
	- But pvr scan failed to work
	- Target is connected to pantahub server api.pantahub.com
	- pantavisor.log, lxc.log shows no error
- tezi: is it initramfs? and how it works?
	- Only basic analysis for tezi is done
	- Above TODO will be cleared before tezi inherit into pantacor

How to build?
- git clone poky
- git clone meta-freescale
- git clone meta-freescale-3rdparty
- git clone meta-pantacor
- source poky build
- use colibri-imx6ull.conf as machine
- build bitbake pv-image and pv-initramfs

How to flash?
- cd build/tmp/work/coli*/pv-image/rootfs_final/
- cp -rfv * /sd/card/ext4/filesystem

How to boot?
- u-boot env is not imported yet, so
- run the following commands,
u-boot commands:
setenv sdargs 'root=/dev/ram rootfstype=ramfs init=/init';
run setup ;
setenv bootargs ${defargs} ${sdargs} ${setupargs} ${vidargs};
load mmc 0:2 ${kernel_addr_r} zImage && load mmc 0:2 ${fdt_addr_r} imx6ull-colibri-eval-v3.dtb && load mmc 0:2 ${ramdisk_addr_r} initramfs && run fdt_fixup && bootz ${kernel_addr_r} ${ramdisk_addr_r} ${fdt_addr_r}

- path for zImage, dtb, initramfs can be trails/0/.pv/

BBB (delta):
------------
- TODO part at imx can be ignored
- ~90% is working
- Follow the same steps to build, flash and boot
- Machine: beaglebone-yocto and samples are under conf/samples
- U-Boot env comes as part of build
