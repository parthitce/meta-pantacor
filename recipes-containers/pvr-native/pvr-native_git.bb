SUMMARY = "Pantavisor Remote control...."
HOMEPAGE = "https://gitlab.com/pantacor/pvr"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b1e01b26bacfc2232046c90a330332b3"

SRC_URI = "git://gitlab.com/pantacor/pvr.git;protocol=https \
	   file://bin/ \
          "
SRCREV = "${AUTOREV}"
inherit native

S = "${WORKDIR}/git"

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/bin/pvr ${D}${bindir}
}

