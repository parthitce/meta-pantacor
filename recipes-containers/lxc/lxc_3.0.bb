SUMMARY = "lxc aims to use these new functionalities to provide an userspace container object"
HOMEPAGE = "https://linuxcontainers.org/"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=4fbd65380cdd255951079008b364516c"

inherit autotools pkgconfig
EXTRA_OECONF += "--disable-api-docs --with-distro=${DISTRO} --disable-werror --enable-bash=no"

SRC_URI = "git://gitlab.com/pantacor/lxc.git;protocol=https;branch=stable-${PV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

do_install_append() {
	install -d ${D}/lib/systemd/system
	install -m 644 ${B}/config/init/systemd/*.service  ${D}/lib/systemd/system
	install -d ${D}/etc/init
	install -m 644 ${B}/config/init/upstart/*.conf  ${D}/etc/init
	cd ${D}/lib
	ln -fs ../usr/lib/liblxc.so.1.4.0 liblxc.so.1
}

PACKAGES =+ "${PN}-templates"

FILES_${PN} += "lib/* etc/*"
FILES_${PN}-templates += "${datadir}/lxc/templates ${datadir}/lxc/hooks"
RDEPENDS_${PN}-templates += "bash"
