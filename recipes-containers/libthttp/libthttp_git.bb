SUMMARY = "tiny httpd from pantacor"
HOMEPAGE = "https://gitlab.com/pantacor/libthttp"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://LICENSE;md5=bd0a4fad56a916f12a1c3cedb3976612"

SRC_URI = "git://gitlab.com/pantacor/${BPN}.git;protocol=https \
	   file://0001-thttp-add-cmake.patch \
          "
DEPENDS = "mbedtls"

SRCREV = "${AUTOREV}"
inherit cmake

#EXTRA_OEMAKE += " BUILDDIR=${S} DESTDIR=${D} CONFIG_PREFIX=${D}/${sysconfdir}"
S = "${WORKDIR}/git"

PACKAGES += " ${PN}-certs"

FILES_${PN}-certs = "/certs/* "
