SUMMARY = "Mbed TLS is a C library that implements cryptographic primitives, X.509 certificate manipulation and the SSL/TLS and DTLS protocols"
HOMEPAGE = "https://gitlab.com/pantacor/mbedtls"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://LICENSE;md5=302d50a6369f5f22efdb674db908167a"

inherit cmake perlnative python3native
SRC_URI = "git://gitlab.com/pantacor/${BPN}.git;protocol=https \
	"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"
CFLAGS_append = " -Wstringop-overflow=0 -Wformat-truncation=0 -Wformat-overflow=0"
