SUMMARY = "dropbear-pv "
HOMEPAGE = "https://gitlab.com/pantacor/dropbear-pv"

inherit cmake
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a5ec40cafba26fc4396d0b550f824e01"

SRC_URI = "git://github.com/pantacor/dropbear-pv.git;protocol=https; \
"

SRCREV = "${AUTOREV}"
