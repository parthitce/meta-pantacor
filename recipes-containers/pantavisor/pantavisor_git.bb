SUMMARY = "Pantavisor is the container-based init system that makes it possible \
	   to enable your static firmware Linux device into a multi-function one."
HOMEPAGE = "https://gitlab.com/pantacor/pantavisor"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://LICENSE;md5=9537d669f1ccfd74f3940b3bc1d3297d"

DEPENDS = "libthttp lxc libpvlogger"
RDEPENDS_${PN} = "lxc libpvlogger"

inherit cmake
SRC_URI = "git://gitlab.com/pantacor/${BPN}.git;protocol=https;name=pantavisor \
           git://gitlab.com/pantacor/pv-config-bbb.git;protocol=https;name=pv-config;destsuffix=config \
	   file://0001-pantavisor-add-CMake-for-bitbake-build.patch \
	   file://0002-pantavisor-open-with-O_CREAT-or-O_TMPFILE.patch \
           file://resolv.conf \
	"
#SRCREV_pantavisor = "${AUTOREV}"
#commit based on Jul31
SRCREV_pantavisor = "4503b6e4356c487957d5b4d451133f7d13a744ce"
SRCREV_pv-config = "${AUTOREV}"
SRCREV_FORMAT = "pantavisor_pv-config"

OECMAKE_GENERATOR = "Unix Makefiles"

S = "${WORKDIR}/git"
CFLAGS_append = " -g -Wno-format-nonliteral -Wno-format-contains-nul -D_FILE_OFFSET_BITS=64 -fPIC -no-pie -DPANTAVISOR_DEBUG"
LDFLAGS +=" -Wl,--no-as-needed -lutil -Wl,--no-as-needed -ldl -Wl,--as-needed -static-libgcc "

do_configure_prepend() {
	${S}/gen_version.sh ${S} ${S}
}

do_install_append() {
        install -d ${D}/etc
	install -d ${D}/lib
        install -m 644 ${WORKDIR}/config/pantavisor.config ${D}/etc/pantavisor.config
        install -m 644 ${WORKDIR}/resolv.conf ${D}/etc/resolv.conf
        install -m 755 pv_lxc.so ${D}/${baselib}
	cd ${D}
	ln -fs usr/bin/${PN} init
	# Move pv_lxc shared library to $base_libdir so the library can be used in early boot.
        mv ${D}/${libdir}/pv_lxc.so ${D}/${base_libdir}
	rmdir usr/lib
}

FILES_${PN} += "init etc/pantavisor.config etc/resolv.conf lib/pv_lxc.so"
