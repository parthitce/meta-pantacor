FILESEXTRAPATHS_append := "${THISDIR}/files:"
SRC_URI += "file://config.cfg"

do_deploy_append () {
	mksquashfs ${D}${root_prefix}/lib/modules/*/ $deployDir/modules-${MODULE_TARBALL_NAME}.squashfs -noappend
	ln -sf modules-${MODULE_TARBALL_NAME}.squashfs $deployDir/modules-${MODULE_TARBALL_LINK_NAME}.squashfs
}

do_deploy[depends] += "squashfs-tools-native:do_populate_sysroot"
