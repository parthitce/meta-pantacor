FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://uboot.txt"

UBOOT_SUFFIX = "img"
SPL_BINARY = "MLO"

DEPENDS +="u-boot-tools-native"

do_environment_mkimage() {
    #to generate uboot environment binary
    mkenvimage -s 131072 -o ${B}/uboot.env ${WORKDIR}/uboot.txt
    
}

do_install_append() {
    install -m 644 ${B}/uboot.env ${D}/boot/uboot.env
}

do_deploy_append() {
    install -m 644 ${B}/uboot.env ${DEPLOYDIR}
}

addtask environment_mkimage after do_compile before do_deploy
