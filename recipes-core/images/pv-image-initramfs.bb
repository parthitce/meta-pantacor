DESCRIPTION = "Basic initramfs image with pantavisor as init"

PACKAGE_INSTALL = "lxc pantavisor libthttp-certs libgcc base-files udev base-passwd ${VIRTUAL-RUNTIME_base-utils} ${ROOTFS_BOOTSTRAP_INSTALL}"

# Do not pollute the initrd image with rootfs features
IMAGE_FEATURES = ""
IMAGE_LINGUAS = ""

LICENSE = "MIT"

# TODO use cpio.xz instead and enable xz in kernel fragment
IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"
DEPENDS = "pvr-native"
inherit core-image

# Un-comment the following 3 lines to consume the pantavisor + initramfs from
# PVR merger source

#inherit core-image pantacor
#PANTACOR_ROOTDIR = "${WORKDIR}/rootfs_pantacor"
#PVR_MERGE_SRC="https://pvr.pantahub.com/pantahub-ci/rpi3_initial_stable"

IMAGE_ROOTFS_SIZE = "8192"
IMAGE_ROOTFS_EXTRA_SPACE = "0"

BAD_RECOMMENDATIONS += "busybox-syslog busybox-udhcpc"
export IMAGE_BASENAME = "pv-image-initramfs"
