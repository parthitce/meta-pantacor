DESCRIPTION = "Basic image for pantacor"
LICENSE = "MIT"

#PACKAGE_INSTALL = " ${VIRTUAL-RUNTIME_base-utils} ${ROOTFS_BOOTSTRAP_INSTALL}"

inherit core-image pantacor
#PVR_MERGE_SRC="https://pvr.pantahub.com/pantahub-ci/arm_generic_bsp_latest/13"

IMAGE_INSTALL = "base-files sysvinit packagegroup-core-boot kernel-modules"

DEPENDS += "pvr-native pv-image-initramfs"

# Do not pollute with rootfs features
IMAGE_FEATURES += "package-management"
IMAGE_LINGUAS = "en-us"

do_pvrinit[depends] = "pv-image-initramfs:do_image_complete"

IMAGE_ROOTFS_SIZE = "8192"
IMAGE_ROOTFS_EXTRA_SPACE = "0"

BAD_RECOMMENDATIONS += "busybox-syslog busybox-udhcpc"
export IMAGE_BASENAME = "pv-image"

#do_pvrservice[noexec] = "1"

#Default initrd & kernel are replaced in reference image '{"initrd":"pantavisor", "linux":"kernel.img"}'
#PVRIMAGE_LIST ='{"linux":"kernel.img"}'

#WKS_FILE = "pv-base-sd.wks"
