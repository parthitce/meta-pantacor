# This class creates file structure for pantacor
# 
# 

IMAGE_FSTYPES += " squashfs"
PANTACOR_STAGINGDIR = "${WORKDIR}/rootfs_staging"
PANTACOR_FINALDIR = "${WORKDIR}/rootfs_final"
PANTACOR_SERVICEDIR = "${PANTACOR_STAGINGDIR}/${BPN}"
PANTACOR_BSPDIR = "${PANTACOR_STAGINGDIR}/bsp"
PANTACOR_BSPTMPDIR = "${PANTACOR_STAGINGDIR}/.tmp"
PVRIMAGE_LIST ?='{"initrd":"pantavisor", "linux":"kernel.img", "modules": "modules.squashfs"}'

#http POST https://api.pantahub.com/auth/login username=user password=pass //to get new token
TOKEN="eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTI3NjEyODQsImlkIjoicGFydGhpdGNlQGdtYWlsLmNvbSIsIm5pY2siOiJwYXJ0aGl0Y2UiLCJvcmlnX2lhdCI6MTU5MjY3NDg4NCwicHJuIjoicHJuOjo6YWNjb3VudHM6LzVjODBlN2UzZGE1ZDQ4MDAwOTM1ODNiYyIsInJvbGVzIjoidXNlciIsInNjb3BlcyI6InBybjpwYW50YWh1Yi5jb206YXBpczovYmFzZS9hbGwiLCJ0eXBlIjoiVVNFUiJ9.vDJ_FlUK4DljJ0AztK71okHc4xgr0MhI-qvonUgHNdz7t80h_kd2iZ8Tls0_7BzYIFCjHLGyQ16rdELXWwBEaS4u6FuL9DtSMKoVaWXrYSuMF3pC7SR1WHpcoM7xyf90l5rswMt3SheSj6Y-UqNHHQUI0G5Zlt2ZXQb0pj-3yXcq0g6z9KDwCYfaLJ4sXbzW6ZD7bUv3lGKKLmpqVp5hVcJN9zjJ0UM1TTZtVI4GXqDnqjsw33fMCMsn4nq-zvXhWefTzp5Av0EZEUBWi3DjQu-scgWPt-P7ZnNrqBDaC80TKPYHF5-BbsWNDRBGn1nNccfHIR84AIhA00-hKlNIUw"

python do_pvrinit() {
    import os, sys, subprocess, shutil, json
    #temp for creating required files, below created files should be provided by build
    try:
        bb.utils.mkdirhier(d.getVar('PANTACOR_STAGINGDIR'))
    except OSError as e:
        bb.error("PANTACOR: Could not set up required directories: " + str(e))
        return
    os.chdir(d.getVar('PANTACOR_STAGINGDIR'))
    bb.utils.mkdirhier(".tmp")
    bb.warn("first func for PVR merge")
    #PVRIMAGE_LIST={"initrd":"pantavisor", "linux":"kernel.img", "modules":"modules.squashfs", "firmware":"firmware.squashfs", "fdt":"fdt.dtb"}
    os.chdir(d.getVar('PANTACOR_STAGINGDIR')) #${WORKDIR}/rootfs_staging

    os.system("pvr init --objects "+d.getVar('PANTACOR_STAGINGDIR')+"/.pvr/objects" )
    with open(d.getVar('PANTACOR_PATH')+'/json/network-mapping.json', 'r') as src, open(d.getVar('PANTACOR_STAGINGDIR')+'/network-mapping.json', 'w') as dst:
        json_content=json.load(src)
        json.dump(json_content, dst)
    with open(d.getVar('PANTACOR_PATH')+'/json/storage-mapping.json', 'r') as src, open(d.getVar('PANTACOR_STAGINGDIR')+'/storage-mapping.json', 'w') as dst:
        json_content=json.load(src)
        json.dump(json_content, dst)
    #process = subprocess.call([cmd ], cwd=d.getVar('PANTACOR_STAGINGDIR'), stdout=subprocess.PIPE, stderr=subprocess.PIPE) //fix OSError: [Errno 36] File name too long
    #stdout, stderr = process.communicate()
    #print(stdout)
    if d.getVar('PVR_MERGE_SRC'):
        cmd="%s %s %s %s %s" % ("pvr", "-a", d.getVar('TOKEN'), "merge", d.getVar('PVR_MERGE_SRC'))
        ret=os.system(cmd)
        if ret == 0:
            print("pvr merge done successfully!")
        else:
            bb.error("PANTACOR: could not get pvr files!")
        process = subprocess.check_output(["pvr checkout"], stdin=None, stderr=None, shell=True)
    process = subprocess.check_output(["pvr commit"], stdin=None, stderr=None, shell=True)
}
addtask pvrinit after do_image_squashfs before do_pvrbsp

python do_pvrbsp() {
    import os, sys, shutil, json, glob
    PVRIMAGE_LIST = json.loads(d.getVar('PVRIMAGE_LIST'))
    print(PVRIMAGE_LIST)
    bb.utils.mkdirhier(d.getVar('PANTACOR_BSPDIR'))
    if d.getVar('PVR_MERGE_SRC') is None:
        #with open(d.getVar('PANTACOR_PATH')+'/bsp/pantavisor.json', 'r') as tmp_json, open(d.getVar('PANTACOR_BSPTMPDIR')+'/pantavisor.json', 'w') as pantavisor_json:
        #    json_content=json.load(tmp_json)
        #    json.dump(json_content, pantavisor_json)
        with open(d.getVar('PANTACOR_PATH')+'/bsp/run.json', 'r') as src, open(d.getVar('PANTACOR_BSPTMPDIR')+'/run.json', 'w') as dst:
            json_content=json.load(src)
            json_content.update(PVRIMAGE_LIST)
            json.dump(json_content, dst)
        with open(d.getVar('PANTACOR_PATH')+'/bsp/src.json', 'r') as src, open(d.getVar('PANTACOR_BSPTMPDIR')+'/src.json', 'w') as dst:
            json_content=json.load(src)
            json.dump(json_content, dst)
    for dtbs in d.getVar('KERNEL_DEVICETREE').split( ):
        shutil.copy(d.getVar('DEPLOY_DIR_IMAGE')+'/'+dtbs, d.getVar('PANTACOR_BSPTMPDIR')+'/')
    
    if 'linux' in PVRIMAGE_LIST:
        print("test")
        shutil.copy(d.getVar('DEPLOY_DIR_IMAGE')+'/uImage', d.getVar('PANTACOR_BSPTMPDIR')+'/kernel.img') 
    
    if 'modules' in PVRIMAGE_LIST:
        print("test")
        shutil.copy(d.getVar('DEPLOY_DIR_IMAGE')+'/modules-'+d.getVar('MACHINE')+'.squashfs', d.getVar('PANTACOR_BSPTMPDIR')+'/modules.squashfs')
    
    if 'initrd' in PVRIMAGE_LIST:
        print("test")
        print(d.getVar('DEPLOY_DIR_IMAGE')+'/'+d.getVar('INITRAMFS_IMAGE')+'-'+d.getVar('MACHINE')+'.'+d.getVar('INITRAMFS_FSTYPES'))
        #for initrd in glob.glob(d.getVar('BASE_WORKDIR')+'/'+d.getVar('MULTIMACH_TARGET_SYS')+'/'+d.getVar('INITRAMFS_IMAGE')+'/*/deploy-*/'+d.getVar('INITRAMFS_IMAGE')+'-'+d.getVar('MACHINE')+'.'+d.getVar('INITRAMFS_FSTYPES')):
         #   print(initrd+"test")
         #   shutil.copy(initrd, d.getVar('PANTACOR_BSPTMPDIR')+'/pantavisor') #ideally only 1 file should be present, if multiple files then last iterated one is copied
        shutil.copy(d.getVar('DEPLOY_DIR_IMAGE')+'/'+d.getVar('INITRAMFS_IMAGE')+'-'+d.getVar('MACHINE')+'.'+d.getVar('INITRAMFS_FSTYPES'), d.getVar('PANTACOR_BSPTMPDIR')+'/pantavisor' )
    
    if 'firmware' in PVRIMAGE_LIST:
        pass #copy firmware
    if 'modules' in PVRIMAGE_LIST:
        pass #copy modules      

    #for file in os.listdir(d.getVar('PANTACOR_BSPDIR')):
    #    shutil.copy(d.getVar('PANTACOR_BSPDIR')+'/'+file, d.getVar('PANTACOR_STAGINGDIR')+'/.tmp')
}
addtask pvrbsp after do_pvrinit before do_pvrservice

python do_pvrservice() {
    import os, sys, shutil, json, glob
    bb.utils.mkdirhier(d.getVar('PANTACOR_SERVICEDIR'))
    for squashfs in glob.glob(d.getVar('IMGDEPLOYDIR')+'/'+'*.rootfs.squashfs'): 
        shutil.copy(squashfs, d.getVar('PANTACOR_SERVICEDIR')+'/root.squashfs')
    with open(d.getVar('PANTACOR_PATH')+'/service/lxc.container.template', 'r')as lxc_template, open(d.getVar('PANTACOR_SERVICEDIR')+'/lxc.container.conf', 'w') as lxc_conf:
        lxc_conf.write(lxc_template.read().replace('${SERVICE_NAME}',d.getVar('BPN')))
    with open(d.getVar('PANTACOR_PATH')+'/service/run.json', 'r') as src, open(d.getVar('PANTACOR_SERVICEDIR')+'/run.json', 'w') as dst:
        json_content=json.load(src)
        json_content['name']=d.getVar('BPN')        
        json.dump(json_content, dst)
    with open(d.getVar('PANTACOR_PATH')+'/service/src.json', 'r') as src, open(d.getVar('PANTACOR_SERVICEDIR')+'/src.json', 'w') as dst:
        json_content=json.load(src)
        json.dump(json_content, dst)
}
addtask pvrservice after do_pvrbsp before do_pvrstaging

python do_pvrstaging () {
    import os, sys, subprocess, shutil, json
    os.chdir(d.getVar('PANTACOR_STAGINGDIR')) #${WORKDIR}/rootfs_staging
    PVRIMAGE_LIST = json.loads(d.getVar('PVRIMAGE_LIST')) 
    #if os.path.isdir(d.getVar('PANTACOR_STAGINGDIR')+"/.pvr/objects" ):
    #    shutil.rmtree(d.getVar('PANTACOR_STAGINGDIR')+"/.pvr/objects" )
#   os.system("pvr init --objects "+d.getVar('PANTACOR_STAGINGDIR')+"/.pvr/objects" )
    #process = subprocess.call([cmd ], cwd=d.getVar('PANTACOR_STAGINGDIR'), stdout=subprocess.PIPE, stderr=subprocess.PIPE) //fix OSError: [Errno 36] File name too long
    #stdout, stderr = process.communicate()
    #print(stdout)
#   if d.getVar('PVR_MERGE_SRC'):
#       cmd="%s %s %s %s %s" % ("pvr", "-a", d.getVar('TOKEN'), "merge", d.getVar('PVR_MERGE_SRC'))
#       ret=os.system(cmd)
#       if ret == 0:
#           print("pvr merge done successfully!")
#       else:
#           bb.error("PANTACOR: could not get pvr files!")
#       process = subprocess.check_output(["pvr checkout"], stdin=None, stderr=None, shell=True)
    #// remove pantavisor.cpio* && bsp/pantavisor.cpio*
    output = subprocess.check_output(["pvr json"], stdin=None, stderr=None, shell=True)
#    print(output)
    resp = json.loads(output.decode('utf-8'))
    if 'pantavisor-multi-platform@1' in resp['#spec']:
        print("to be implemented")
    else:
        if os.path.isdir('.tmp'):
            #os.system("pwd")
            with open(d.getVar('PANTACOR_BSPTMPDIR')+'/run.json', 'r') as f:
                basejson = json.load(f)
            for element in PVRIMAGE_LIST:
                if os.path.isfile('.tmp/'+PVRIMAGE_LIST[element]):
                    filesrc=os.path.join(d.getVar('PANTACOR_STAGINGDIR')+'/.tmp/', PVRIMAGE_LIST[element])
                    dst=d.getVar('PANTACOR_STAGINGDIR')+'/bsp/'
                    shutil.copy(filesrc, dst) #+PVRIMAGE_LIST[element])
                    #os.remove(filesrc)
                    #shutil.move(os.path.join(d.getVar('PANTACOR_STAGINGDIR')+'/.tmp/', PVRIMAGE_LIST[element]), os.path.join(d.getVar('PANTACOR_STAGINGDIR'),'bsp'))
                    basejson[element] = PVRIMAGE_LIST[element]
            with open(d.getVar('PANTACOR_STAGINGDIR')+'/bsp/run.json', 'w') as f:
                json.dump(basejson, f)

    #for file in os.listdir(d.getVar('PANTACOR_STAGINGDIR')+'/.tmp/'):
    #    os.remove(d.getVar('PANTACOR_STAGINGDIR')+'/.tmp/'+file)
    process = subprocess.check_call(["pvr add; pvr commit"], stdin=None, stderr=None, shell=True)
    #process = subprocess.check_output(["pvr commit"], stdin=None, stderr=None, shell=True)
}
addtask pvrstaging after do_pvrservice before do_pvrfinal

python do_pvrfinal() {
    import os, sys, shutil, json, glob
    datadir=d.getVar('PANTACOR_FINALDIR')+'/trails/0'
    metadir=d.getVar('PANTACOR_FINALDIR')+'/trails/0/.pv'
    objectdir=d.getVar('PANTACOR_FINALDIR')+'/objects'
    bootdir=d.getVar('PANTACOR_FINALDIR')+'/boot'
    configdir=d.getVar('PANTACOR_FINALDIR')+'/config'
    pvrjson=datadir+'/.pvr/json'
    pvrconfig=datadir+'/.pvr/config'
    try:
        bb.utils.mkdirhier(d.getVar('PANTACOR_FINALDIR'))
        bb.utils.mkdirhier(datadir)
        bb.utils.mkdirhier(metadir)
        bb.utils.mkdirhier(objectdir)
        bb.utils.mkdirhier(bootdir)
        bb.utils.mkdirhier(configdir)
        bb.utils.mkdirhier(datadir+'/.pvr')
    except OSError as e:
        bb.error("PANTACOR: Could not set up required directories: " + str(e))
        return
    bb.utils.mkdirhier(datadir+'/.pvr')
    #shutil.copy(d.getVar('PANTACOR_STAGINGDIR')+'/.pvr/objects', d.getVar('PANTACOR_FINALDIR')) #//other files json, config, commitmsg are not copied as they are deleted at end
    for objects in os.listdir(d.getVar('PANTACOR_STAGINGDIR')+'/.pvr/objects'):
        shutil.copy(d.getVar('PANTACOR_STAGINGDIR')+'/.pvr/objects/'+objects, objectdir) 
    shutil.copy(d.getVar('PANTACOR_STAGINGDIR')+'/.pvr/json', pvrjson) 
    with open(pvrconfig, 'w') as f:
        f.write('{ "ObjectsDir": "../../objects" }')
    with open(d.getVar('PANTACOR_STAGINGDIR')+'/.pvr/json', 'r') as f:
        jsondata = json.load(f)
    print(jsondata)
    for key in jsondata:
        if os.path.isfile(d.getVar('PANTACOR_STAGINGDIR')+"/"+key) and isinstance(jsondata[key], str):
            filename=datadir+"/"+key
            bb.utils.mkdirhier(os.path.dirname(filename))
            if ".bind" in key:  #// need to check further if echo $filename | grep -q \.bind$;
                print("copy files")
            else:
                try:
                    os.link(d.getVar('PANTACOR_STAGINGDIR')+'/.pvr/objects/'+jsondata[key], filename)
                except FileExistsError:
                    os.remove(filename)
                    os.link(d.getVar('PANTACOR_STAGINGDIR')+'/.pvr/objects/'+jsondata[key], filename)
        elif os.path.isfile(d.getVar('PANTACOR_STAGINGDIR')+"/"+key) and isinstance(jsondata[key], dict):        
            filename=datadir+"/"+key
            bb.utils.mkdirhier(os.path.dirname(filename))
            with open(filename, 'w') as f:
                json.dump(jsondata[key], f)
        elif key == '#spec':
            print(jsondata[key])
            if 'pantavisor-multi-platform@1' in jsondata[key]:
                #bspfile=d.getVar('PANTACOR_STAGINGDIR')+"/pantavisor.json" #Clarification needed
                #bspprefix=""
                bspfile=d.getVar('PANTACOR_STAGINGDIR')+"/bsp/run.json"
                bspprefix="bsp"
            else:
                bspfile=d.getVar('PANTACOR_STAGINGDIR')+"/bsp/run.json"
                bspprefix="bsp"
        
    with open(bspfile, 'r') as f:
        bspjson = json.load(f)
    for key in bspjson:
        if key == 'linux' :
            try:
                os.link(datadir+"/"+bspprefix+"/"+bspjson[key], metadir+"/pv-kernel.img")
            except FileExistsError:
                os.remove(metadir+"/pv-kernel.img")
                os.link(datadir+"/"+bspprefix+"/"+bspjson[key], metadir+"/pv-kernel.img")
        elif key == 'initrd' :
            try:
                os.link(datadir+"/"+bspprefix+"/"+bspjson[key], metadir+"/pv-initrd.img")
            except FileExistsError:
                os.remove(metadir+"/pv-initrd.img")
                os.link(datadir+"/"+bspprefix+"/"+bspjson[key], metadir+"/pv-initrd.img")

    for dtb in glob.glob(d.getVar('PANTACOR_BSPDIR')+'/*.dtb'):
        shutil.copy(dtb, metadir)

    with open(d.getVar('PANTACOR_PATH')+'/config/pantahub.config', 'r')as file, open(configdir+'/pantahub.config', 'w') as config_file:
        config_file.write(file.read())
    with open(d.getVar('PANTACOR_PATH')+'/boot/uboot.txt', 'r')as file, open(bootdir+'/uboot.txt', 'w') as boot_file:
        boot_file.write(file.read())

    for file in os.listdir(d.getVar('PANTACOR_FINALDIR')+"/objects"):    #// for removing old objects
        with open(d.getVar('PANTACOR_STAGINGDIR')+'/.pvr/json', 'r') as f:
            if file not in f.read():
                print(file)
                os.remove(d.getVar('PANTACOR_FINALDIR')+"/objects/"+file)
    print("Factory state:\n")
    print(jsondata)
}
#addtask pvrfinal after do_pvrstaging before do_populate_lic_deploy
addtask pvrfinal after do_pvrstaging before do_image_wic
